//
//  WeatherError.swift
//  OpenWeatherApp
//
//  Created by Vikash Kr Chaubey on 23/05/21.
//

import Foundation

enum WeatherError: Error {
  case parsing(description: String)
  case network(description: String)
}

